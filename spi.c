/* */

#include <avr/io.h>  
#include <stdint.h>
#include "spi.h"

void spi_init(void)
{
  DDRB |= ((1<<DDRB4)|(1<<DDRB5)|(1<<DDRB7)); 	/* spi pins on port B MOSI SCK outputs */

  SPCR = ((1<<SPE)|(1<<MSTR)|(1<<CPOL)|(1<<CPHA)|(1<<SPR0));  /* SPI enable, Master, f/16, polarity and phase = 1 */
}

uint8_t spi_transmit(uint8_t cData)
{
   SPDR = cData;
   while(!(SPSR & (1<<SPIF)))
      ;

   return SPDR;
}	