/* */

#ifndef PERIPHERY_H_
#define PERIPHERY_H_

#include <stdint.h>
#include <stdio.h>

void periphery_init(void);

void periphery_toggle_led1(void);

void periphery_toggle_led2(void);

void periphery_toggle_led3(void);

void periphery_set_rgb(uint8_t out_num, uint8_t r, uint8_t g, uint8_t b);

#endif /* PERIPHERY_H_ */