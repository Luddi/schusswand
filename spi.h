/* */

#ifndef SPI_H_
#define SPI_H_

#include <stdint.h>

void spi_init(void);

uint8_t spi_transmit(uint8_t cData);

#endif /* SPI_H_ */