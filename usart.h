/* */

#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include <stdio.h>

void usart_init(uint16_t ubrr);

void usart_putchar(char data);

char usart_getchar(void);

unsigned char usart_kbhit(void);

void usart_pstr(char *s);

int usart_putchar_printf(char var, FILE *stream);

#endif /* UART_H_ */