/* */

#include <avr/io.h>  
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include <stdio.h>
#include "mems.h"
#include "spi.h"
#include "periphery.h"
#include "logic.h"

/* Interrupt Service Routine for INT0 */
ISR(INT0_vect)
{
	logic_hit_detect();
}

/* Interrupt Service Routine for INT1 */
ISR(INT1_vect)
{
	logic_hit_detect();
}

/* Interrupt Service Routine for INT2 */
ISR(INT2_vect)
{
	logic_hit_detect();
}

/* Polled ISR for for INT3 */
void mems_poll_int3(void)
{
	static bool prev = false;

	if ((PINB & (1 << PB3)) && !prev) {	/* rising edge */
		logic_hit_detect();

		prev = true;
	}
	if (!(PINB & (1 << PB3))) {			/* falling edge */
		prev = false;
	}
}

void mems_enable_int(void)
{
	DDRD &= ~((1<<PD2)|(1<<PD3));							/* Set PD2 and PD3 as input */
	DDRB &= ~((1<<PB2)|(1<<PB3));							/* Set PB2 and PB3 as input */

	GICR = (1<<INT0)|(1<<INT1)|(1<<INT2);					/* Enable INT */
	MCUCR = (1<<ISC01)|(1<<ISC00)|(1<<ISC11)|(1<<ISC10);	/* Trigger INT on rising edge */
	MCUCSR = (1<<ISC2);
}

/**
 * Initializes mems sensor
 * @return
 */
void mems_init_sensor(void)
{
	uint8_t confBuf = 0x00;
	int i;

	/* enable CS Pins */
	DDRA |= (1 << PA0);
	DDRA |= (1 << PA1);
	DDRA |= (1 << PA2);
	DDRA |= (1 << PA3);

	for (i = 0; i < 4; ++i) {
		/** configure ctrl register 1 */
		confBuf = IIS328DQ_NORMAL_MODE|IIS328DQ_ODR1000|IIS328DQ_ZEN;
		mems_tx(i, IIS328DQ_CTRL_REG1, &confBuf, 1);

		/** configure ctrl register 2 */
		confBuf = IIS328DQ_HPM_NORMAL|IIS328DQ_FDS|IIS328DQ_HPC8|IIS328DQ_HPEN1;
		mems_tx(i, IIS328DQ_CTRL_REG2, &confBuf, 1);

		/** configure ctrl register 3 */
		confBuf = IIS328DQ_LIR1;
		mems_tx(i, IIS328DQ_CTRL_REG3, &confBuf, 1);

		/** configure ctrl register 4 */
		confBuf = IIS328DQ_BDU|IIS328DQ_FS8G;
		mems_tx(i, IIS328DQ_CTRL_REG4, &confBuf, 1);		

		/** configure int 1 cfg register */
		confBuf = IIS328DQ_ZHIE;
		mems_tx(i, IIS328DQ_INT1_CFG, &confBuf, 1);
	
	}

	mems_enable_int();
}


/**
 * @brief Write to mems sensor
 * @param address. This is the address field of the indexed register.
 * @param data_out. Pointer to byte array of data to write.
 * @param len. Number of bytes to transmit
 * @return
 */
void mems_tx(uint8_t sens_num, uint8_t address, uint8_t *data_out, uint8_t len)
{
	int i;

	mems_set_cs(sens_num, true);
	spi_transmit(address & ~0x80);
	for(i=0; i<len; i++) {
		spi_transmit(data_out[i]);
	}
	mems_set_cs(sens_num, false);
}

/**
 * @brief Read from mems sensor
 * @param address. This is the address field of the indexed register.
 * @param data_out. Pointer to byte array of data to write.
 * @param len. Number of bytes to read
 * @return
 */
void mems_rx(uint8_t sens_num, uint8_t address, uint8_t *data_in, uint8_t len)
{
	int i;

	mems_set_cs(sens_num, true);
	spi_transmit(address | 0x80);
	for (i = 0; i < len; ++i) {
		data_in[i] = spi_transmit(0xFF);
	}
	mems_set_cs(sens_num, false);
}

/**
 * Check for working sensor
 * @return bool: sensor present.
 */
bool mems_check_presence(uint8_t sensor_num)
{
	uint8_t rx_buf = 0;

	mems_rx(sensor_num, IIS328DQ_WHO_AM_I_REG, &rx_buf, 1);

	if (rx_buf == IIS328DQ_WHO_AM_I) {
		return true;
	}
	
	return false;
}

/**
 * Set CS line
 * @param sensor_num. from {0,1,2,3}
 * @param status. true = CS low
 * @return
 */
void mems_set_cs(uint8_t sensor_num, bool status)
{
	if (status) {
		PORTA &= ~(1 << sensor_num);
	} else {
		PORTA |= (1 << sensor_num);
	}
}

/**
 * Set threshold and duration for interrupt generation on all sensors
 * @param threshold. from 0 to 8000mg
 * @param duration. minimum duation from 0 to 127ms
 * @return
 */
void mems_set_intparams(uint8_t threshold, uint8_t duration)
{
	uint8_t confBuf = 0x00;
	int i;

	for (i=0; i<4; i++) {
		/** configure int 1 threshold register */
		confBuf = (threshold) & 0xFF;
		mems_tx(i, IIS328DQ_INT1_THS, &confBuf, 1);

		/** configure int 1 duration register */
		confBuf = duration;
		mems_tx(i, IIS328DQ_INT1_DURATION, &confBuf, 1);
	}
}

/**
 * Reads int 1 register to clear latched interrupt,
 * needed if ctrl 3 is configured with IIS328DQ_LIR1
 * @param sensor_num. from {0,1,2,3}
 * @return contents of int 1 register
 */
uint8_t mems_clear_int(uint8_t sensor_num)
{
	uint8_t confBuf = 0x00;
    mems_rx(sensor_num, IIS328DQ_INT1_SRC, &confBuf, 1);

    return confBuf;
}

/**
 * 
 * @param sensor_num. from {0,1,2,3}
 * @return 
 */
int16_t mems_get_z(uint8_t sensor_num)
{
	uint8_t readBuf[2];
	int16_t z;

    mems_rx(sensor_num, IIS328DQ_AUTO_INC|IIS328DQ_OUT_Z_L, readBuf, 2);

    z = (((int16_t) readBuf[1]) << 8) + (int16_t) readBuf[0];

    /** Sensitivity factor = fullscale/2^15 = 0.244 ~1/4 */
    z = z >> 2;

    return z;
}