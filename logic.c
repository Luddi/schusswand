/* */

#include <stdio.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "logic.h"
#include "mems.h"
#include "periphery.h"

#define ABS(x) ((x)<0 ? -(x) : (x))

#define AREA_CNT 4
#define DELTAT 1			/* time in ms to pass between sensor readouts */
#define DELTACNT1 (7*DELTAT)
#define BUFSIZE 200			/* number of samples to buffer for comparison */
#define LIGHT_DURATION 6	/* light up winning field for time=LIGHT_DURATION*356ms */
#define POSTINT_SAMPLES 150 /* number of samples to take after interrupt was asserted, has to be < BUFSIZE */

enum logic_state {
	NORMAL,
	INT_PENDING,
	WAITING
};

static volatile uint32_t slow_systick = 6;	/* systick timer in multiples of 356ms */

static volatile uint32_t hit_time;
static volatile bool int_pending = false;	/* checked if one sensor has triggered an interrupt */

static bool sensor_present[AREA_CNT];	/* presence of sensor gets checked at init */

static int16_t z_buf[AREA_CNT][BUFSIZE];


static bool sample_sensors(void);
static uint32_t get_rm(int16_t *buf, uint16_t count);
static uint32_t get_sum_envelope(int16_t *buf, uint16_t count, uint8_t window_size);

/* ISR counts from 0 to 4 -> 28.125Hz/5 => T=356ms */
ISR (TIMER2_OVF_vect)
{
	static uint8_t pwm_cnt = 0;

	pwm_cnt++;				/* increment counter */
	if (pwm_cnt > 4) {		/* increment systick */
		pwm_cnt = 0;
		slow_systick++;
	}
}

void logic_init(void)
{
	uint8_t i = 0;

	/* config 8 bit timer 2 as slow systick */
	TCCR2 = (1<<CS20)|(1<<CS21)|(1<<CS22); /* prescaler 1024 -> 7372800Hz/1024/256 = 28.125Hz,
											division of 256 because of 8 bit overflow */
	/* initialize counter */
    TCNT2 = 0;
 	/* enable overflow interrupt */
	TIMSK |= (1<<TOIE2);

	/* config 16 bit timer 1 as fast systick */
	TCCR1B |= (1<<CS12)|(1<<CS10);		/* prescaler 1024 -> 7372800Hz/1024 = 7.2kHz, wraparound after 9.1s */
	/* initialize counter */
    TCNT1 = 0;

	/* check for connection of sensors */
	for (i = 0; i < AREA_CNT; ++i) {
		if ( (sensor_present[i] = mems_check_presence(i)) ) { 
			periphery_set_rgb(i, 1, 1, 1);
			printf("Sensor %d present\n", i);
		} else {
			printf("Sensor %d not present!\n", i);
		}
	}
}

/**
 * Indicate that any area has been hit
 * Correct area is computed in handler
 * @return
 */
void logic_hit_detect()
{
	int_pending = true;
	periphery_toggle_led3();
}


/**
 * Main handler for logic to be called in loop
 */
void logic_handler(void)
{
	static enum logic_state status = NORMAL;
	uint8_t hit_area;
	uint16_t i;
	uint32_t rm[AREA_CNT] = {0};

	switch (status) {
		case NORMAL:
			/* normal operation, get new samples */
			sample_sensors();

			if (int_pending) {
				status = INT_PENDING;
			}
			break;
		case INT_PENDING:
			i = 0;
			while (i < POSTINT_SAMPLES) { 	/* loop while getting post interrupt samples */
				if (sample_sensors()) {
					i++;
				}
			}

			for (i = 0; i < AREA_CNT; ++i) {
				if (!sensor_present[i]) {
					continue;	/* skip buffer if sensor not present */
				}
				//rm[i] = get_rm(z_buf[i], BUFSIZE);
                rm[i] = get_sum_envelope(z_buf[i], BUFSIZE, 5);
			}

			hit_area = 0;
			printf("0: %"PRIu32, rm[0]>>14);
			for (i = 1; i < AREA_CNT; ++i) {	/* determine greatest value */
				printf(", %d: %"PRIu32, (int)i, rm[i]>>14);
				if (rm[i] > rm[hit_area]) {
					hit_area = i;
				}
			}

            for (i = 0; i < AREA_CNT; ++i) {	/* turn off all areas */
				periphery_set_rgb(i, 0, 0, 0);
			}

			periphery_set_rgb(hit_area, 0, 1, 0);
			printf("\r\nArea %d hit!\r\n", hit_area);
			hit_time = slow_systick;

			status = WAITING;
			break;
		case WAITING:
			if ((hit_time + LIGHT_DURATION) < slow_systick) {		/* check if time passed to resume data acqusition */
				/* clear interrupts in all sensors, reset light, TODO: check if present */
				for (i = 0; i < AREA_CNT; ++i) {
					mems_clear_int(i);
					periphery_set_rgb(i, 1, 1, 1);
				}
				int_pending = false;
				printf("Resume.\n");

				status = NORMAL;
			}
			break;
		}
}

/**
 * read out sensors and write into circular z_buf
 * @return true if new samples have been aqcuired, false if not enough time passed
 */
static  bool sample_sensors(void)
{
	uint16_t i;
	static uint16_t cnt1 = 0;
	static uint16_t bufptr = 0;

	if (cnt1 > TCNT1) {	/* handle wraparound of counter */
		cnt1 = 0;
	}
	/* check if enough time passed for new sample */
	if (TCNT1 < (cnt1 + DELTACNT1)) {
		return false;			
	} else {
		cnt1 = TCNT1;
	}

	for (i = 0; i < AREA_CNT; ++i)
	{
		if (sensor_present[i]) {
			z_buf[i][bufptr] = mems_get_z(i);

			if (++bufptr >= BUFSIZE) {	/* increment pointer and wrap around at end of buffer */
				bufptr = 0;
			}
		}
	}	

	return true;
}

/**
 * computes root mean (without square) of values in buf of length count
 */
static uint32_t get_rm(int16_t *buf, uint16_t count)
{
    uint32_t rm = 0, sq;
    int i;

    for (i = 0; i < count; ++i) {
        sq = (uint32_t)((int32_t)buf[i] * (int32_t)buf[i]);
        rm += sq;
    }

    return rm;
}

/**
 * computes sum of evelope of values in buf of length count
 * window_size wide non overlapping windows are applied
 */
static uint32_t get_sum_envelope(int16_t *buf, uint16_t count, uint8_t window_size)
{
    uint32_t sum = 0;
    int16_t max;
    int i, j;

    /* make values positive */
    for (i = 0; i < count; ++i) {
        buf[i] = ABS(buf[i]);
    }

    /* assign maximum in window to all values in window */
    for (i = window_size; i <= count; i += window_size) {
        /* find maximum */
        for (j = -window_size, max = buf[i+j], ++j; j < 0; ++j) {
            max = (buf[i+j] > max) ? buf[i+j] : max;
        }
        /* apply maximum */
        for (j = -window_size; j < 0; ++j) {
            buf[i+j] = max;
        }
    }

    /* sum up values */
    for (i = 0; i < count; ++i) {
        sum += buf[i];
    }

    return sum;
}