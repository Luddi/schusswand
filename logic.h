/* */

#ifndef LOGIC_H_
#define LOGIC_H_

void logic_init(void);

void logic_hit_detect(void);

void logic_handler(void);

#endif /* LOGIC_H_ */