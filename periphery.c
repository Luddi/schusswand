/* */

#include <avr/io.h>
#include <avr/interrupt.h>
#include "periphery.h"

#define PWM_PIN_COUNT 12

typedef struct pwm_pin {
	volatile uint8_t * port;
	uint8_t pin;
} pwm_pin_t;

static pwm_pin_t pwm_pins[PWM_PIN_COUNT] = {
	{ .port = &PORTC, .pin = PC0},	/* r1 */
	{ .port = &PORTC, .pin = PC1},	/* g1 */
	{ .port = &PORTC, .pin = PC2},	/* b1 */
	{ .port = &PORTC, .pin = PC3},	/* r2 */
	{ .port = &PORTC, .pin = PC4},	/* ... */
	{ .port = &PORTC, .pin = PC5},
	{ .port = &PORTC, .pin = PC6},
	{ .port = &PORTC, .pin = PC7},
	{ .port = &PORTB, .pin = PB0},
	{ .port = &PORTD, .pin = PD7},
	{ .port = &PORTD, .pin = PD6},
	{ .port = &PORTD, .pin = PD5}
};

void periphery_init(void)
{
	/* enable LED Pins */
	DDRA |= (1 << PA5)|(1 << PA6)|(1 << PA7);

	/* enable RGB outputs */
	DDRC = 0xFF;			/* all PORTC outputs */
	DDRB |= (1 << PB0);
	DDRD |= (1 << PD5)|(1 << PD6)|(1 << PD7);
}

void periphery_toggle_led1(void)
{
	PORTA ^= (1 << PA5);
}

void periphery_toggle_led2(void)
{
	PORTA ^= (1 << PA6);
}

void periphery_toggle_led3(void)
{
	PORTA ^= (1 << PA7);
}

/*
 * Sets RGB value of output 1,2,3 or 4
 * @param r,g,b turn channel on/off
 * @return
 */
void periphery_set_rgb(uint8_t out_num, uint8_t r, uint8_t g, uint8_t b)
{
	if (out_num > 3) {
		return;
	}

	/* set / reset pin */
	if (r) {
		*(pwm_pins[out_num*3].port) |= (1 << pwm_pins[out_num*3].pin);
	} else {
		*(pwm_pins[out_num*3].port) &= ~(1 << pwm_pins[out_num*3].pin);
	}
	
	/* set / reset pin */
	if (g) {
		*(pwm_pins[out_num*3+1].port) |= (1 << pwm_pins[out_num*3+1].pin);
	} else {
		*(pwm_pins[out_num*3+1].port) &= ~(1 << pwm_pins[out_num*3+1].pin);
	}

	/* set / reset pin */
	if (b) {
		*(pwm_pins[out_num*3+2].port) |= (1 << pwm_pins[out_num*3+2].pin);
	} else {
		*(pwm_pins[out_num*3+2].port) &= ~(1 << pwm_pins[out_num*3+2].pin);
	}

}