/* */

#ifndef MEMS_H_
#define MEMS_H_

#include <stdint.h>
#include <stdbool.h>

/************** I2C Address *****************/
#define IIS328DQ_I2C_ADDRESS_LOW   (0x18<<0x1U)  /**< SAD[0] = 0 */
#define IIS328DQ_I2C_ADDRESS_HIGH  (0x19<<0x1U)  /**< SAD[0] = 1 */

/************** Who am I  *******************/
#define IIS328DQ_WHO_AM_I_REG     0x0F
#define IIS328DQ_WHO_AM_I         0x32

/************** Device Register  *******************/
#define IIS328DQ_CTRL_REG1    0x20
#define IIS328DQ_CTRL_REG2    0x21
#define IIS328DQ_CTRL_REG3    0x22
#define IIS328DQ_CTRL_REG4    0x23
#define IIS328DQ_CTRL_REG5    0x24
#define IIS328DQ_HP_FILTER_RESET    0x25
#define IIS328DQ_REFERENCE    0x26
#define IIS328DQ_STATUS_REG    0x27
#define IIS328DQ_OUT_X_L    0x28
#define IIS328DQ_OUT_X_H    0x29
#define IIS328DQ_OUT_Y_L    0x2A
#define IIS328DQ_OUT_Y_H    0x2B
#define IIS328DQ_OUT_Z_L    0x2C
#define IIS328DQ_OUT_Z_H    0x2D
#define IIS328DQ_INT1_CFG    0x30
#define IIS328DQ_INT1_SRC    0x31
#define IIS328DQ_INT1_THS    0x32
#define IIS328DQ_INT1_DURATION    0x33
#define IIS328DQ_INT2_CFG    0x34
#define IIS328DQ_INT2_SRC    0x35
#define IIS328DQ_INT2_THS    0x36
#define IIS328DQ_INT2_DURATION    0x37

/************** Device Register BITS *******************/
#define IIS328DQ_POWER_DOWN       0x0
#define IIS328DQ_NORMAL_MODE      (1 << 5)
#define IIS328DQ_LP_2             (0x4U << 5)
#define IIS328DQ_LP_5             (0x5U << 5)
#define IIS328DQ_LP_10            (0x6U << 5)
#define IIS328DQ_ODR50            0x0
#define IIS328DQ_ODR100           (1 << 3)
#define IIS328DQ_ODR400           (1 << 4)
#define IIS328DQ_ODR1000          (0x3U << 3)
#define IIS328DQ_ZEN              (1 << 2)
#define IIS328DQ_YEN              (1 << 1)
#define IIS328DQ_XEN              (1 << 0)

#define IIS328DQ_HPM_NORMAL       0x0
#define IIS328DQ_HPM_REF          (1 << 5)
#define IIS328DQ_FDS              (1 << 4)
#define IIS328DQ_HPEN1            (1 << 2)
#define IIS328DQ_HPEN2            (1 << 3)
#define IIS328DQ_HPC8             0x0
#define IIS328DQ_HPC16            0x1
#define IIS328DQ_HPC32            0x2
#define IIS328DQ_HPC64            0x3

#define IIS328DQ_IHL              (1 << 7)
#define IIS328DQ_PP_OD            (1 << 6)
#define IIS328DQ_LIR1             (1 << 2)
#define IIS328DQ_I1_CFG_OR        (0x3U)

#define IIS328DQ_BDU              (1 << 7)
#define IIS328DQ_FS2G             0x0
#define IIS328DQ_FS4G             (1 << 4)
#define IIS328DQ_FS8G             (0x3U << 4)

#define IIS328DQ_AOI              (1 << 7)
#define IIS328DQ_6D               (1 << 6)
#define IIS328DQ_ZHIE             (1 << 5)
#define IIS328DQ_ZLIE             (1 << 4)

/**
 *  If the MSb of the SUB field is ‘1’, the SUB (register address)
 *  is automatically increased to allow multiple data read/write.
 */
#define IIS328DQ_AUTO_INC         (1 << 6)


void mems_poll_int3(void);

void mems_init_sensor(void);

void mems_tx(uint8_t sens_num, uint8_t address, uint8_t *data_out, uint8_t len);

void mems_rx(uint8_t sens_num, uint8_t address, uint8_t *data_in, uint8_t len);

bool mems_check_presence(uint8_t sensor_num);

void mems_set_cs(uint8_t sensor_num, bool status);

void mems_set_intparams(uint8_t threshold, uint8_t duration);

uint8_t mems_clear_int(uint8_t sensor_num);

int16_t mems_get_z(uint8_t sensor_num);

#endif /* MEMS_H_ */