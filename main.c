 /* Ball hit indicator */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdbool.h>
#include "spi.h"
#include "usart.h"
#include "mems.h"
#include "periphery.h"
#include "logic.h"

/* usart settings */
#define BAUDRATE 19200
#define MYUBRR F_CPU/16/BAUDRATE-1

#define INT_THS 60  /* threshold for interrupt generation: 
                       threshold/fullscale * (2^7 - 1)    */
#define INT_DUR 5   /* minimum duration for threshold to trigger interrupt */

int main(void) {

  periphery_init();
  usart_init(MYUBRR);
  
  printf("Ball hit indicator V0.2\r\n");

  spi_init();
  mems_init_sensor();
  mems_set_intparams(INT_THS, INT_DUR);
  logic_init();

  /* clear interrupts in sensors */
  for (int i = 0; i < 4; ++i) {
    mems_clear_int(i);
  }

  sei();    /* Enable Global Interrupt */

  while (1) 
  {
    mems_poll_int3();
    logic_handler();
    periphery_toggle_led1();
  }
  
  return (0);
}
